package com.zanglikun.selector;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @Author ：zanglk
 * @DateTime ：2022/10/29 01:57
 * @Description ：@Import注入方式二：实现ImportSelector接口重写selectImports方法
 * @Notes ：To change this template：Click IDEA-Preferences to search 'File Templates'
 */
public class DefaultImportSelector implements ImportSelector {
    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        //return new String[0];
        return new String[]{"com.zanglikun.entity.DiyUser"}; // 指定IOC容器加载此Bean
    }
}
