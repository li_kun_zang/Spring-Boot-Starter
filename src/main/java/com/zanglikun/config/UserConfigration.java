package com.zanglikun.config;

import com.zanglikun.regist.DefaultImportBeanDefinitionRegistrar;
import com.zanglikun.selector.DefaultImportSelector;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @Author ：zanglk
 * @DateTime ：2022/10/28 11:37
 * @Description ：暂无说明
 * @Notes ：To change this template：Click IDEA-Preferences to search 'File Templates'
 */
//@Configuration
//@EnableConfigurationProperties({User.class})
//@Import({User.class}) // @Import注入方式一：直接注册Bean
@Import({DefaultImportBeanDefinitionRegistrar.class}) // 方式二：写一个类实现ImportSelector接口，需将自己类加入到IOC容器
public class UserConfigration {
}
