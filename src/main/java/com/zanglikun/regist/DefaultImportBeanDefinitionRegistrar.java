package com.zanglikun.regist;

import com.zanglikun.entity.DiyUser;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @Author ：zanglk
 * @DateTime ：2022/10/29 02:04
 * @Description ：@Import注入方式三：实现ImportBeanDefinitionRegistrar接口重写registerBeanDefinitions方法
 * @Notes ：To change this template：Click IDEA-Preferences to search 'File Templates'
 */
public class DefaultImportBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {

    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        // ImportBeanDefinitionRegistrar.super.registerBeanDefinitions(importingClassMetadata, registry);
        RootBeanDefinition rootBeanDefinition = new RootBeanDefinition(DiyUser.class); // 配置Bean
        registry.registerBeanDefinition("userInstance",rootBeanDefinition); // 注册Bean
    }
}
