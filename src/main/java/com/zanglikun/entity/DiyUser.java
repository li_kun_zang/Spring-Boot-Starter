package com.zanglikun.entity;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/**
 * @Author ：zanglk
 * @DateTime ：2022/10/28 11:33
 * @Description ：读取配置文件
 * @Notes ：To change this template：Click IDEA-Preferences to search 'File Templates'
 */
@Data
@ConfigurationProperties("user.zhangsan") // 读取配置文件的内容
public class DiyUser {
    /**
     * 姓名
     */
    private String name;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 爱好
     */
    private String[] hobbits;
}
