package com.zanglikun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author ：zanglk
 * @DateTime ：2022/10/28 11:20
 * @Description ：暂无说明
 * @Notes ：To change this template：Click IDEA-Preferences to search 'File Templates'
 */
@SpringBootApplication
public class RunApplication {

    public static void main(String[] args) {
        SpringApplication.run(RunApplication.class,args);
    }
}
