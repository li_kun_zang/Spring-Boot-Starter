import com.zanglikun.RunApplication;

import com.zanglikun.entity.DiyUser;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;


@ExtendWith(SpringExtension.class) // 使用Junit5测试工具
@WebAppConfiguration // 启动web运行环境
@SpringBootTest(classes = RunApplication.class) // 编写测试类
@Slf4j
public class RunTest {

    @Autowired(required = false)
    private DiyUser user;

    @Test
    public void testRun() {
        System.out.println(user);
    }

}
